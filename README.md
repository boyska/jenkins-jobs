# Tails jenkins jobs

Tails is using OpenStack's jenkins-job-builder to configure jobs in Jenkins.

For more on how to configure them using this repo, see
https://docs.openstack.org/infra/jenkins-job-builder/

The `test.sh` script displays the diff between the jobs' XML generated from the
master branch and the one generated from a development branch:

    ./test.sh development_branch

It is useful to see what changes in the configuration will really be sent to
our online Jenkins instance once compiled by jenkins-job-builder, and avoid
mistakes like a job being deleted because of a typo.
