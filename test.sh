#!/bin/sh
#
# Show the diff between the jobs xml files generated from the master branch
# code and the one from a branch given as a command line argument.
#

set -e

if [ -z "$1" ]; then
	echo "Err: specify a branch to compare with master's jobs xml."
	exit 1
fi

GITHEAD="$1"
GITOLDHEAD="master"
CURRENT_PATH=$(pwd)
TMPDIR=$(mktemp -d /tmp/jenkins_jobs_XXXXXXXX)

trap cleanup EXIT

cleanup() {
	[ -d "$TMPDIR" ] && rm -rf "$TMPDIR"
	git checkout "$GITHEAD"
}

generate_xmls() {
	for rev in "$GITOLDHEAD" "$GITHEAD"; do
		mkdir --parents "${TMPDIR}/${rev}" || exit 1
		git checkout "$rev"
		jenkins-jobs test -o "${TMPDIR}/${rev}" "$CURRENT_PATH"
	done
}

show_xmls_diff () {
	generate_xmls
	diff -Naur "${TMPDIR}/${GITOLDHEAD}/" "${TMPDIR}/${GITHEAD}"
}

show_xmls_diff
